package io.srm.mapper;

import io.srm.domain.dto.CustomerDTO;
import io.srm.domain.entity.Customer;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerMapper {

    public Optional<Collection<CustomerDTO>> toDto(Collection<Customer> entities) {
        final Optional<Collection<Customer>> maybeEntities = Optional.ofNullable(entities == null || entities.isEmpty() ? null : entities);

        return maybeEntities
                .map(e -> e.stream()
                        .map(this::toDto)
                        .map(Optional::get)
                        .sorted(Comparator.comparing(CustomerDTO::getName))
                        .collect(Collectors.toCollection(LinkedList::new)));
    }

    public Optional<CustomerDTO> toDto(Customer entity) {
        final Optional<Customer> maybeEntity = Optional.ofNullable(entity);

        return maybeEntity
                .map(e -> {
                    final CustomerDTO dto = new CustomerDTO();

                    dto.setId(e.getId());
                    dto.setName(e.getName());
                    dto.setCreditLimit(e.getCreditLimit());
                    dto.setRisk(e.getRisk());

                    //noinspection Duplicates
                    switch (e.getRisk()) {
                        case B:
                            dto.setInterestRate(10.0);
                            break;
                        case C:
                            dto.setInterestRate(20.0);
                            break;
                    }

                    return dto;
                });
    }

    public Optional<Collection<Customer>> toEntity(Collection<CustomerDTO> dtos) {
        final Optional<Collection<CustomerDTO>> maybeDtos = Optional.ofNullable(dtos == null || dtos.isEmpty() ? null : dtos);

        return maybeDtos
                .map(d -> d.stream()
                        .map(this::toEntity)
                        .map(Optional::get)
                        .collect(Collectors.toCollection(LinkedList::new)));
    }

    public Optional<Customer> toEntity(CustomerDTO dto) {
        final Optional<CustomerDTO> maybeDto = Optional.ofNullable(dto);

        return maybeDto
                .map(d -> {
                    final Customer entity = new Customer();

                    entity.setId(d.getId());
                    entity.setName(d.getName());
                    entity.setCreditLimit(d.getCreditLimit());
                    entity.setRisk(d.getRisk());

                    //noinspection Duplicates
                    switch (d.getRisk()) {
                        case B:
                            entity.setInterestRate(10.0);
                            break;
                        case C:
                            entity.setInterestRate(20.0);
                            break;
                    }

                    return entity;
                });
    }

}
