package io.srm.exception;

public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String source;

    private String code;

    public BusinessException(final String message) {
        super(message);
    }

    public BusinessException(final Throwable throwable) {
        super(throwable);
    }

    public BusinessException(final Throwable throwable, final String message) {
        super(message, throwable);
    }

    public BusinessException(final String message, final String source, final String code) {
        super(message);

        this.source = source;
        this.code = code;
    }

    public BusinessException(final Throwable throwable, final String source, final String code) {
        super(throwable.getMessage(), throwable);

        this.source = source;
        this.code = code;
    }

    public String getSource() {
        return source;
    }

    public String getCode() {
        return code;
    }
}
