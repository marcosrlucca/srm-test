package io.srm.exception;

public class NotFoundException extends RuntimeException {

    private String source;

    private String code;

    public NotFoundException(final String message) {
        super(message);
    }

    public NotFoundException(final Throwable throwable) {
        super(throwable);
    }

    public NotFoundException(final Throwable throwable, final String message) {
        super(message, throwable);
    }

    public NotFoundException(final String message, final String source, final String code) {
        super(message);

        this.source = source;
        this.code = code;
    }

    public NotFoundException(final Throwable throwable, final String source, final String code) {
        super(throwable.getMessage(), throwable);

        this.source = source;
        this.code = code;
    }

    public String getSource() {
        return source;
    }

    public String getCode() {
        return code;
    }
}