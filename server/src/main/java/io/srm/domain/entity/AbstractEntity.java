package io.srm.domain.entity;

import java.io.Serializable;

public abstract class AbstractEntity<ID extends Serializable & Comparable<ID>> implements Serializable {
    private static final long serialVersionUID = -2820768155445338200L;

    public ID id;

    abstract public ID getId();

    public void setId(ID id) throws IllegalStateException {
        if (id != null && !id.equals(id)) {
            throw new IllegalStateException("Can't change id from " + this.id + " to " + id);
        }

        this.id = id;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[id=" + id + "]";
    }
}
