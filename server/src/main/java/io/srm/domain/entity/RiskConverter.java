package io.srm.domain.entity;

import io.srm.domain.enums.Risk;
import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Optional;

@Converter
public class RiskConverter implements AttributeConverter<Risk, String> {

    @Override
    public String convertToDatabaseColumn(Risk value) {
        return Optional.ofNullable(value)
                .map(Enum::toString)
                .orElse(null);
    }

    @Override
    public Risk convertToEntityAttribute(String value) {
        return Optional.ofNullable(value)
                .filter(v -> !StringUtils.isEmpty(v))
                .map(Risk::valueOf)
                .orElse(null);
    }
}

