package io.srm.domain.enums;

public enum Risk implements IStringEnum<Integer> {
    A(0), B(10), C(20);

    private Integer value;

    Risk(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
