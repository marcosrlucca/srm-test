package io.srm.domain.dto;

import io.srm.domain.enums.Risk;

public class CustomerDTOBuilder {
    private Long id;
    private String name;
    private Double creditLimit;
    private Double interestRate;
    private Risk risk;

    public CustomerDTOBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public CustomerDTOBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CustomerDTOBuilder setCreditLimit(Double creditLimit) {
        this.creditLimit = creditLimit;
        return this;
    }

    public CustomerDTOBuilder setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
        return this;
    }

    public CustomerDTOBuilder setRisk(Risk risk) {
        this.risk = risk;
        return this;
    }

    public CustomerDTO build() {
        CustomerDTO c = new CustomerDTO();
        c.setInterestRate(interestRate);
        c.setRisk(risk);
        c.setCreditLimit(creditLimit);
        c.setName(name);
        c.setId(id);
        return c;
    }
}
