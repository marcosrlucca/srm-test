package io.srm.service;

import io.srm.domain.dto.CustomerDTO;
import io.srm.domain.entity.Customer;
import io.srm.exception.BusinessException;
import io.srm.exception.NotFoundException;
import io.srm.mapper.CustomerMapper;
import io.srm.repository.CustomerRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final static Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private final CustomerRepository repository;
    private final CustomerMapper mapper;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository, CustomerMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Optional<Collection<CustomerDTO>> findAll() {
        return mapper.toDto(repository.findAll());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Either<Throwable, Optional<CustomerDTO>> saveOrUpdate(CustomerDTO dto) {
        return Try.of(() -> {
            final Optional<CustomerDTO> maybeDto = Optional.ofNullable(dto);

            return maybeDto.flatMap(d -> {
                if (!validate(d)) throw new BusinessException("The customer is not valid");

                final Optional<Customer> maybeEntity = mapper.toEntity(d);

                return maybeEntity.flatMap(e -> {

                    if (d.getId() == null) {
                        final Optional<Customer> maybeEntityAlreadyExists = Optional.ofNullable(repository.findByName(d.getName()));

                        if (maybeEntityAlreadyExists.isPresent()) throw new BusinessException("Entity already exists");

                        return mapper.toDto(repository.saveAndFlush(e));
                    }

                    final Optional<Customer> maybeEntityFromDB = Optional.ofNullable(repository.findOne(e.getId()));

                    if (maybeEntityFromDB.isPresent()) {
                        final Optional<Customer> maybeEntityAlreadyExists = Optional.ofNullable(repository.findByName(d.getName()));

                        if (maybeEntityAlreadyExists.isPresent()) throw new BusinessException("Entity already exists");
                    } else {
                        throw new NotFoundException("No customer found to update");
                    }

                    return mapper.toDto(repository.saveAndFlush(e));
                });
            });
        })
                .onSuccess(a -> LOGGER.info("Successfully updated"))
                .onFailure(a -> LOGGER.info("Error when updating"))
                .toEither();
    }

    private Boolean validate(CustomerDTO dto) {
        return (!StringUtils.isEmpty(dto.getName()) && dto.getCreditLimit() != null && dto.getRisk() != null);
    }

}
