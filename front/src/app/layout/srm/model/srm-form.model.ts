export class Customer {
    id: number;
    name: string;
    risk: string;
    creditLimit: string;
    interestRate: string;
}
