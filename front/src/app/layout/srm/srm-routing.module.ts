import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SRMComponent} from './srm.component';
import {CustomerComponent} from './customer/customer.component';

const routes: Routes = [
    {
        path: '', component: SRMComponent
    },
    {
        path: 'customer', component: CustomerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SRMRoutingModule {
}
