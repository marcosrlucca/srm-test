import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-srm',
    templateUrl: './srm.html',
    styleUrls: ['./srm.scss']
})
export class SRMComponent implements OnInit {

    constructor(public router: Router) {
    }

    ngOnInit(): void {
    }

    goToCustomer() {
        this.router.navigate(['/srm/customer']);
    }
}
